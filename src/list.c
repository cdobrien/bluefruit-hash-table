#include "list.h"
#include <stdlib.h>
#include <string.h>

static List *FindNodeByKey(List *root, const char *keyToFind);

List List_New()
{
    Pair pair =
    {
        .key = NULL,
        .value = NULL
    };
    List list =
    {
        .pair = pair,
        .next = NULL
    };
    return list;
}

bool List_Append(List *root, Pair pair)
{
    if (pair.value == NULL)
    {
        return false;
    }

    if (root->pair.key == NULL)
    {
        root->pair = pair;
        return true;
    }

    if (root->next == NULL)
    {
        root->next = malloc(sizeof(List));
    }

    return List_Append(root->next, pair);
}

const char *List_FindValueByKey(List *root, const char *keyToFind)
{
    List *foundNode = FindNodeByKey(root, keyToFind);
    if (foundNode == NULL)
    {
        return NULL;
    }
    return foundNode->pair.value;
}

bool List_DeleteByKey(List *root, const char *keyToDelete)
{
    if (root->next == NULL)
    {
        return false;
    }

    if (strcmp(root->next->pair.key, keyToDelete) == 0)
    {
        List *newNext = root->next->next;
        free(root->next);
        root->next = newNext;
        return true;
    }

    return List_DeleteByKey(root->next, keyToDelete);
}

bool List_UpdateValueByKey(List *root, const char *keyToUpdate, const char *newValue)
{
    List *nodeToUpdate = FindNodeByKey(root, keyToUpdate);
    if (nodeToUpdate == NULL)
    {
        return false;
    }
    nodeToUpdate->pair.value = newValue;
    return true;
}

static List *FindNodeByKey(List *root, const char *keyToFind)
{
    if (strcmp(root->pair.key, keyToFind) == 0)
    {
        return root;
    }

    if (root->next == NULL)
    {
        return NULL;
    }

    return FindNodeByKey(root->next, keyToFind);
}
