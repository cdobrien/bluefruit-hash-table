#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "testmacros.h"
#include "list.h"

TEST_GROUP(ListTests)
{
    TEST(can_append_1_node_to_a_new_list)
    {
        Pair pair = {
            .key = "foo",
            .value = "bar"
        };

        List root = List_New();
        bool succeeded = List_Append(&root, pair);

        ASSERT_TRUE(succeeded);
        ASSERT_NOT_EQUAL(root.pair.key, NULL);
        ASSERT_NOT_EQUAL(root.pair.value, NULL);
        ASSERT_EQUAL(0, strcmp(root.pair.key, "foo"));
        ASSERT_EQUAL(0, strcmp(root.pair.value, "bar"));
    }

    TEST(can_append_3_nodes_to_a_new_list)
    {
        Pair pair1 = {
            .key = "foo",
            .value = "bar"
        };

        Pair pair2 = {
            .key = "baz",
            .value = "quuuxxxxx"
        };

        Pair pair3 = {
            .key = "sna",
            .value = "fu"
        };

        List root = List_New();
        bool succeeded = List_Append(&root, pair1);
        ASSERT_TRUE(succeeded);

        succeeded = List_Append(&root, pair2);
        ASSERT_TRUE(succeeded);

        succeeded = List_Append(&root, pair3);
        ASSERT_TRUE(succeeded);

        List *pair1Node = &root;
        List *pair2Node = pair1Node->next;
        List *pair3Node = pair2Node->next;

        ASSERT_EQUAL(0, strcmp(pair1Node->pair.key, pair1.key));
        ASSERT_EQUAL(0, strcmp(pair1Node->pair.value, pair1.value));

        ASSERT_EQUAL(0, strcmp(pair2Node->pair.key, pair2.key));
        ASSERT_EQUAL(0, strcmp(pair2Node->pair.value, pair2.value));

        ASSERT_EQUAL(0, strcmp(pair3Node->pair.key, pair3.key));
        ASSERT_EQUAL(0, strcmp(pair3Node->pair.value, pair3.value));
    }

    TEST(can_find_a_node_by_key)
    {
        const char *keyToFind = "baz";
        const char *valueToFind = "quuuxxxxx";

        Pair pair1 = {
            .key = "foo",
            .value = "bar"
        };

        Pair pair2 = {
            .key = keyToFind,
            .value = valueToFind,
        };

        Pair pair3 = {
            .key = "sna",
            .value = "fu"
        };

        List root = List_New();
        (void)List_Append(&root, pair1);
        (void)List_Append(&root, pair2);
        (void)List_Append(&root, pair3);

        const char *foundValue = List_FindValueByKey(&root, keyToFind);
        ASSERT_EQUAL(valueToFind, foundValue);
    }

    TEST(cannot_append_a_pair_with_a_null_value)
    {
        Pair nullPair = {
            .key = "foo",
            .value = NULL
        };

        List root = List_New();

        bool success = List_Append(&root, nullPair);

        ASSERT_FALSE(success);
        ASSERT_EQUAL(NULL, root.pair.key);
    }

    TEST(can_delete_a_node)
    {
        const char *keyToDelete = "baz";
        const char *valueToDelete = "quuuxxxxx";

        Pair pair1 = {
            .key = "foo",
            .value = "bar"
        };

        Pair pair2 = {
            .key = keyToDelete,
            .value = valueToDelete,
        };

        Pair pair3 = {
            .key = "sna",
            .value = "fu"
        };

        List root = List_New();
        (void)List_Append(&root, pair1);
        (void)List_Append(&root, pair2);
        (void)List_Append(&root, pair3);

        bool deleteSuccessful = List_DeleteByKey(&root, keyToDelete);

        ASSERT_TRUE(deleteSuccessful);
        const char *foundValue = List_FindValueByKey(&root, keyToDelete);
        ASSERT_EQUAL(NULL, foundValue);
    }

    TEST(can_update_a_value_for_a_key)
    {
        const char *keyToUpdate = "baz";
        const char *newValue = "quuuxxxxx";

        Pair pair1 = {
            .key = "foo",
            .value = "bar"
        };

        Pair pair2 = {
            .key = keyToUpdate,
            .value = "qux",
        };

        Pair pair3 = {
            .key = "sna",
            .value = "fu"
        };

        List root = List_New();
        (void)List_Append(&root, pair1);
        (void)List_Append(&root, pair2);
        (void)List_Append(&root, pair3);

        bool updateSuccessful = List_UpdateValueByKey(&root, keyToUpdate, newValue);

        ASSERT_TRUE(updateSuccessful);
        const char *foundValue = List_FindValueByKey(&root, keyToUpdate);
        ASSERT_EQUAL(newValue, foundValue);
    }
}
