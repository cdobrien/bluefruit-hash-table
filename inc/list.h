#ifndef LIST_H
#define LIST_H

#include <stdbool.h>
#include "pair.h"

typedef struct List {
    Pair pair;
    struct List *next;
} List;

List List_New(void);
bool List_Append(List *root, Pair pair);
const char *List_FindValueByKey(List *root, const char *keyToFind);
bool List_DeleteByKey(List *root, const char *keyToDelete);
bool List_UpdateValueByKey(List *root, const char *keyToUpdate, const char *newValue);

#endif
