#ifndef PAIR_H
#define PAIR_H

typedef struct {
    const char *key;
    const char *value;
} Pair;

#endif
